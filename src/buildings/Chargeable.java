package buildings;

public interface Chargeable {
    public void charge(double price, int periodCount, String type);
}
