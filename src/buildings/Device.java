package buildings;

import java.io.Serializable;

public class Device implements Serializable, Chargeable {
    private static final long serialVersionUID = 1L;
    private double bill = 0;
    private String name = null;
    private String type = null;

    public Device(String name, String type) {
        this.name = name;
        this.type = type;
    }
    double getBill() {
    	return this.bill;
    }
    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return name + " [bill=" + bill + ", name=" + name + ", type=" + type
                + "]";
    }

    @Override
    public void charge(double price, int periodCount, String type) {
        if (this.type.equals(type)) {
            this.bill += price * periodCount;
        }
    }

}
