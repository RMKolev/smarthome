package buildings;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Building implements Chargeable, Serializable {
	private static final long serialVersionUID = 2388817972408723735L;
	private String name;
	private HashMap<String, Device> devices;

	public Building(String name) {
		this.name = name;
		this.devices = new HashMap<String, Device>();
	}

	@Override
	public void charge(double price, int periodCount, String type) {
		this.devices.forEach((k, v) -> v.charge(price, periodCount, type));
	}

	public void addDevice(String name, String type) {
		this.devices.put(name, new Device(name, type));
	}

	public String getName() {
		return this.name;
	}

	public void print() {
		this.devices.forEach((k, v) -> System.out.println(v));
	}

	public double printSum(String type) {
		double sum = 0;
		for (Map.Entry<String, Device> entry : this.devices.entrySet()) {
			Device value = entry.getValue();
			if (value.getType().equals(type)) {
				sum += value.getBill();
			}
		}
		return sum;
	}

	public void showDeviceBill(String name2, String type) {
		if (this.devices.containsKey(name2) && this.devices.get(name2).getType().equals(type)) {
			System.out.println(name2 + ":" + this.devices.get(name2).getBill());
		}

	}

}
