package exceptions;

public class InvalidPriviledgeException extends SystemException {
	private static final long serialVersionUID = 1L;

	public InvalidPriviledgeException() {
		super();
	}

	public InvalidPriviledgeException(String message) {
		super(message);
	}

}
