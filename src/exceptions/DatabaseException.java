package exceptions;

public class DatabaseException extends SystemException {

	private static final long serialVersionUID = -1845773185335215452L;

	public DatabaseException() {
		super();
	}

	public DatabaseException(String message) {
		super(message);
	}
}
