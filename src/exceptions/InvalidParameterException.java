package exceptions;

public class InvalidParameterException extends SystemException {

	public InvalidParameterException(String string) {
		super(string);
	}

	private static final long serialVersionUID = -7480381486890278385L;

}
