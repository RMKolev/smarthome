package exceptions;

public class SystemException extends Exception {
	private static final long serialVersionUID = -5131515151445353378L;

	public SystemException() {
		super();
	}

	public SystemException(String message) {
		super(message);
	}
}
