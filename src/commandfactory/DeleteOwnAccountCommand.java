package commandfactory;

import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.UserAccessParameters;
import system.UserSystem;

public class DeleteOwnAccountCommand extends Command {
	DeleteOwnAccountCommand() {
		super();
	}

	DeleteOwnAccountCommand(String parameters) throws InvalidParameterException {
		this.parameters = new UserAccessParameters(parameters);
	}

	@Override
	public void execute(UserSystem s) throws SystemException {
		s.removeCurrentUserFromDatabase();
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidPriviledgeException, InvalidParameterException {
		if (priviledge.equals("Guest")) {
			throw new InvalidPriviledgeException("You are not logged in!");
		}
		return new DeleteOwnAccountCommand(parameters);
	}

}
