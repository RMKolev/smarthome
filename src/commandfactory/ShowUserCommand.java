package commandfactory;

import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.UserAccessParameters;
import system.UserSystem;

public class ShowUserCommand extends Command {
	public ShowUserCommand() {
		super();
	}
	public ShowUserCommand(String parameters) throws InvalidParameterException {
		this.parameters = new UserAccessParameters(parameters);
	}

	@Override
	public void execute(UserSystem s) throws SystemException {
		s.showUser(((UserAccessParameters)this.parameters).getName());
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		if(priviledge.equals("Guest")) {
			throw new InvalidPriviledgeException("You are not logged in.");
		}
		return new ShowUserCommand(parameters);
	}

}
