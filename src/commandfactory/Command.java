package commandfactory;

import parameters.Parameters;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import system.UserSystem;

public abstract class Command {
	protected Parameters parameters;

	public abstract void execute(UserSystem s) throws SystemException;

	public abstract Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException;
}
