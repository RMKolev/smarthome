package commandfactory;

import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.MoveTimeParameters;
import system.UserSystem;

public class MovePeriodCommand extends Command{
	public MovePeriodCommand() {
		
	}
	public MovePeriodCommand(String parameters) throws InvalidParameterException {
		this.parameters = new MoveTimeParameters(parameters);
	}

	@Override
	public void execute(UserSystem s) throws SystemException {
		s.movePeriod(((MoveTimeParameters)this.parameters).getPeriodCount());
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		return new MovePeriodCommand(parameters);
	}
	
}
