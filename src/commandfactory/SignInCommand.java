package commandfactory;

import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.UserValidationParameters;
import system.UserSystem;

public class SignInCommand extends Command{
	
	SignInCommand(){
		super();
	}
	SignInCommand(String parameters) throws InvalidParameterException{
		this.parameters = new UserValidationParameters(parameters);
	}
	@Override
	public void execute(UserSystem s) throws SystemException {
		s.logInUser(((UserValidationParameters)this.parameters).getName(),((UserValidationParameters)this.parameters).getPassword());
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge) throws InvalidParameterException, InvalidPriviledgeException {
		if (!priviledge.equals("Guest")) {
			throw new InvalidPriviledgeException("You are already logged in!");
		}
		return new SignInCommand(parameters);
	}

}
