package commandfactory;

import java.util.HashMap;

import commandfactory.addcommandfactory.AddCommandFactory;
import commandfactory.showcommandfactory.ShowCommandFactory;
import exceptions.DatabaseException;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;

public class CommandFactory {
	private static HashMap<String, Command> commands;
	static {
		commands = new HashMap<String, Command>();
		commands.put("signUp", new SignUpCommand());
		commands.put("signIn", new SignInCommand());
		commands.put("logOut", new LogOutCommand());
		commands.put("deleteMyProfile", new DeleteOwnAccountCommand());
		commands.put("showUser", new ShowUserCommand());
		commands.put("show", new ShowCommandFactory());
		commands.put("add", new AddCommandFactory());
		commands.put("set", new SetPriceCommand());
		commands.put("move", new MovePeriodCommand());
	}

	public static Command get(String command, String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException, DatabaseException {
		if(!commands.containsKey(command)) {
			throw new DatabaseException("No such command");
		}
		return commands.get(command).getCopyOf(parameters,priviledge);
	}
}
