package commandfactory.addcommandfactory;

import commandfactory.Command;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.UserAccessParameters;
import system.UserSystem;

public class AddSmartBuildingCommand extends Command {
	public AddSmartBuildingCommand() {
		
	}
	public AddSmartBuildingCommand(String parameters) throws InvalidParameterException {
		this.parameters = new UserAccessParameters(parameters);
	}
	@Override
	public void execute(UserSystem s) throws SystemException {
		s.addBuilding(((UserAccessParameters)this.parameters).getName());

	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		return new AddSmartBuildingCommand(parameters);
	}

}
