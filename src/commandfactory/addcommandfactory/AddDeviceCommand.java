package commandfactory.addcommandfactory;

import commandfactory.Command;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.AddDeviceParameters;
import parameters.UserAccessParameters;
import system.UserSystem;

public class AddDeviceCommand extends Command {
	AddDeviceCommand(){
		
	}
	AddDeviceCommand(String params) throws InvalidParameterException{
		this.parameters = new AddDeviceParameters(params);
	}
	@Override
	public void execute(UserSystem s) throws SystemException {
		AddDeviceParameters  params= (AddDeviceParameters)this.parameters;
		s.addSmartDevice(params.getBuildingName(), params.getName(), params.getType());
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		return new AddDeviceCommand(parameters);
	}

}
