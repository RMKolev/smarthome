package commandfactory.addcommandfactory;

import java.util.HashMap;

import commandfactory.Command;
import system.UserSystem;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.AddCommandParameters;
import parameters.UserAccessParameters;

public class AddCommandFactory extends Command {
	static HashMap<String, Command> subcommands;
	static {
		subcommands = new HashMap<String, Command>();
		subcommands.put("device", new AddDeviceCommand());
		subcommands.put("building", new AddSmartBuildingCommand());
	}

	public AddCommandFactory() {
		super();
	}

	@Override
	public void execute(UserSystem fs) throws SystemException {
		throw new SystemException("AddCommandFactory command executed");
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		this.parameters = new AddCommandParameters(parameters);
		if (!this.subcommands.containsKey(((AddCommandParameters) this.parameters).getSpecification())) {
			throw new InvalidParameterException("No such command");
		} else {
			return this.subcommands.get(((AddCommandParameters) this.parameters).getSpecification())
					.getCopyOf(((AddCommandParameters) this.parameters).getParameters(), priviledge);
		}
	}

}