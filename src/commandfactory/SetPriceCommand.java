package commandfactory;

import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.SetPriceParameters;
import system.UserSystem;

public class SetPriceCommand extends Command{
	public SetPriceCommand() {
		
	}
	public SetPriceCommand(String parameters) throws InvalidParameterException {
		this.parameters = new SetPriceParameters(parameters);
	}
	@Override
	public void execute(UserSystem s) throws SystemException {
		s.setBill(((SetPriceParameters)this.parameters).getType(),((SetPriceParameters)this.parameters).getPrice());
		
	}
	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		return new SetPriceCommand(parameters);
	}
}
