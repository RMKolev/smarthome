package commandfactory.showcommandfactory;

import commandfactory.Command;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.ShowBillParameters;
import parameters.UserAccessParameters;
import system.UserSystem;

public class ShowBillCommand extends Command {
	ShowBillCommand(){
		
	}
	ShowBillCommand(String parameters) throws InvalidParameterException{
		this.parameters = new ShowBillParameters(parameters);
	}
	@Override
	public void execute(UserSystem s) throws SystemException {
		
		s.showBill(((ShowBillParameters)this.parameters).getName(),((ShowBillParameters)this.parameters).getType());
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		return new ShowBillCommand(parameters);
	}

}
