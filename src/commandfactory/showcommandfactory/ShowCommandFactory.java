package commandfactory.showcommandfactory;

import java.util.HashMap;

import commandfactory.Command;
import system.UserSystem;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import parameters.DisplayParameters;

public class ShowCommandFactory extends Command {
	static HashMap<String, Command> subcommands;
	static {
		subcommands = new HashMap<String, Command>();
		subcommands.put("buildings", new ShowBuildingsCommand());
		subcommands.put("devices", new ShowDevicesCommand());
		subcommands.put("bill", new ShowBillCommand());
	}

	@Override
	public void execute(UserSystem fs) throws SystemException {
		throw new SystemException("ShowCommandFactory command executed");
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		DisplayParameters p = new DisplayParameters(parameters);
		if(this.subcommands.containsKey(p.getType())) {
			return this.subcommands.get(p.getType()).getCopyOf(p.getParameters(), priviledge);
		}
		else {
			throw new InvalidParameterException("No such command");
		}
	}

}
