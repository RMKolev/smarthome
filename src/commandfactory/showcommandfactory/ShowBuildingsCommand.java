package commandfactory.showcommandfactory;

import commandfactory.Command;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import system.UserSystem;

public class ShowBuildingsCommand extends Command{

	@Override
	public void execute(UserSystem s) throws SystemException {
		s.printAllBuildings();
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		return new ShowBuildingsCommand();
	}

}
