package commandfactory.showcommandfactory;

import commandfactory.Command;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import system.UserSystem;

public class ShowDevicesCommand extends Command {

	@Override
	public void execute(UserSystem s) throws SystemException {
		s.displayAllDevices();
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		// TODO Auto-generated method stub
		return new ShowDevicesCommand();
	}

}
