package commandfactory;

import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import system.UserSystem;

public class LogOutCommand extends Command {
	LogOutCommand() {
		super();
	}

	@Override
	public void execute(UserSystem s) throws SystemException {
		s.logOutUser();
	}

	@Override
	public Command getCopyOf(String parameters, String priviledge)
			throws InvalidParameterException, InvalidPriviledgeException {
		if (priviledge.equals("Guest")) {
			throw new InvalidPriviledgeException("You are not logged in!");
		}
		return new LogOutCommand();
	}

}
