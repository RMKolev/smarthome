package parameters;

import exceptions.InvalidParameterException;

public class UserAccessParameters implements Parameters {
	String name;

	public String getName() {
		return this.name;
	}

	public UserAccessParameters() {
		super();
	}

	public UserAccessParameters(String parameters) throws InvalidParameterException {
		this.parseString(parameters);
	}

	@Override
	public void parseString(String parameters) throws InvalidParameterException {
		this.name = parameters.trim();
	}
}
