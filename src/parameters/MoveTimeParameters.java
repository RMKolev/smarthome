package parameters;

import exceptions.InvalidParameterException;

public class MoveTimeParameters implements Parameters{
	int timeCount = 0;
	public int getPeriodCount() {
		return this.timeCount;
	}
	public MoveTimeParameters(String parameters) throws InvalidParameterException {
		this.parseString(parameters);
	}
	@Override
	public void parseString(String c) throws InvalidParameterException {
		if(c.startsWith("time")) {
			c = c.replace("time", "").trim();
			this.timeCount = Integer.parseInt(c);
			if(this.timeCount<=0) {
				throw new InvalidParameterException("Invalid command parameters. Time count cannot be less or equal to 0");
			}
		}
	else throw new InvalidParameterException("No such command");
		
	}
}
