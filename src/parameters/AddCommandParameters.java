package parameters;

import exceptions.InvalidParameterException;

public class AddCommandParameters implements Parameters{
	String specification = null;
	String parameters = null;
	AddCommandParameters(){
		
	}
	public AddCommandParameters(String params) throws InvalidParameterException{
		this.parseString(params);
	}
	public String getSpecification() {
		return this.specification;
	}
	public String getParameters() {
		return this.parameters;
	}
	@Override
	public void parseString(String c) throws InvalidParameterException {
		if(c.startsWith("device ")) {
			this.specification = "device";
			this.parameters = c.replace("device ","");
		}
		else if(c.startsWith("smart building ")) {
			this.specification = "building";
			this.parameters = c.replace("smart building ", "");
		}
		else throw new InvalidParameterException("Invalid command parameters");
	}
	
}
