package parameters;

import exceptions.InvalidParameterException;

public class ShowBillParameters implements Parameters {
	String name = null;
	String type = null;

	@Override
	public void parseString(String c) throws InvalidParameterException {
		if (c.startsWith("water")) {
			this.type = "water";
			this.name = c.replaceAll("water", "").trim();
		} else if (c.startsWith("electricity")) {
			this.type = "electricity";
			this.name = c.replaceAll("electricity", "").trim();
		} else {
			throw new InvalidParameterException("Invalid command parameters");
		}
	}

	public ShowBillParameters(String parameters) throws InvalidParameterException {
		this.parseString(parameters);
	}

	public String getType() {
		return this.type;
	}

	public String getName() {
		return this.name;
	}
}
