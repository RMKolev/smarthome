package parameters;

import exceptions.InvalidParameterException;

public class DisplayParameters implements Parameters {
	private String type = null;
	private String parameters = null;

	public String getType() {
		return this.type;
	}
	public String getParameters() {
		return this.parameters;
	}
	public DisplayParameters(String params) throws InvalidParameterException {
		this.parseString(params);
	}

	@Override
	public void parseString(String c) throws InvalidParameterException {
		if (c.startsWith("all buildings")) {
			this.type = "buildings";
		} else if (c.startsWith("all devices")) {
			this.type = "devices";
		} else if (c.startsWith("bill")) {
			this.type = "bill";
			this.parameters = c.replace("bill ", "").trim();
		} else
			throw new InvalidParameterException("No such command");
	}

}
