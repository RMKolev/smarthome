package parameters;

import exceptions.InvalidParameterException;

//To be used for sign-up and logging in
public class UserValidationParameters implements Parameters {
	String name;
	String password;

	public UserValidationParameters() {

	}

	public UserValidationParameters(String parameters) throws InvalidParameterException {
		this.parseString(parameters);
	}

	public String getName() {
		return this.name;
	}

	public String getPassword() {
		return this.password;
	}

	@Override
	public void parseString(String c) throws InvalidParameterException {
		String[] segments = c.split(" ");
		String name = null;
		String password = null;

		for (int i = 0; i < segments.length; i++) {
			segments[i] = segments[i].trim();
			if (segments[i].startsWith("password:")) {
				password = segments[i].replace("password:", "");
			}
			if (segments[i].startsWith("username:")) {
				name = segments[i].replace("username:", "");
			}
		}
		if (name == null || password == null) {
			throw new InvalidParameterException("Invalid command parameters");
		} else {
			this.name = name;
			this.password = password;
		}
	}
}
