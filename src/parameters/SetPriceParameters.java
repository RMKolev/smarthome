package parameters;

import exceptions.InvalidParameterException;

public class SetPriceParameters implements Parameters {
	String type = null;
	double price = 0.0;

	public SetPriceParameters(String parameters) throws InvalidParameterException {
		this.parseString(parameters);
	}

	@Override
	public void parseString(String c) throws InvalidParameterException {
		if (c.startsWith("price")) {
			c = c.replace("price ", "");
			if (c.startsWith("water")) {
				this.type = "water";
				this.price = Double.parseDouble(c.trim().replace("water", ""));
				if (this.price <= 0) {
					throw new InvalidParameterException(
							"Invalid command parameters. Price cannot be less or equal to 0");
				}
			} else if (c.startsWith("electricity")) {
				this.type = "electricity";
				this.price = Double.parseDouble(c.trim().replace("electricity", ""));
				if (this.price <= 0) {
					throw new InvalidParameterException(
							"Invalid command parameters. Price cannot be less or equal to 0");
				}
			} else
				throw new InvalidParameterException("Invalid command parameters");
		} else
			throw new InvalidParameterException("No such command");
	}

	public String getType() {
		return this.type;
	}

	public double getPrice() {
		return this.price;
	}

}
