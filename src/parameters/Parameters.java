package parameters;

import exceptions.InvalidParameterException;

public interface Parameters {
	void parseString(String c) throws InvalidParameterException;
}
