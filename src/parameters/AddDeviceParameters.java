package parameters;

import exceptions.InvalidParameterException;

public class AddDeviceParameters implements Parameters {
	String name = null;
	String type = null;
	String buildingName = null;
	public String getName() {
		return this.name;
	}
	public String getType() {
		return this.type;
	}
	public String getBuildingName() {
		return this.buildingName;
	}
	AddDeviceParameters(){
		
	}
	public AddDeviceParameters(String params) throws InvalidParameterException{
		this.parseString(params);
	}
	@Override
	public void parseString(String c) throws InvalidParameterException {
		String[] parts = c.trim().split(" ");
		for(int i=0;i<parts.length;i++) {
			if(parts[i].equals("water")||parts[i].equals("electricity")) {
				this.type = parts[i];
				StringBuilder name = new StringBuilder();
				StringBuilder building = new StringBuilder();
				for(int j=0;j<i;j++) {
					name.append(" ");
					name.append(parts[j].trim());
				}
				for(int j=i+1;j<parts.length;j++) {
					building.append(" ");
					building.append(parts[j]);
				}
				if(name.toString().equals(null)||building.toString().equals(null)) {
					throw new InvalidParameterException("Invalid command parameters");
				}
				else {
					this.name = name.toString().trim();
					this.buildingName = building.toString().trim();
					return;
				}
			}
		}
		throw new InvalidParameterException("No such command");
	}

}
