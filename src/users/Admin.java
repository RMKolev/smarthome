package users;

public class Admin extends RegisteredUser {

	private static final long serialVersionUID = 1L;

	public Admin(String name, String password) {
		super(name, password);
	}

	@Override
	public String getTitle() {
		return "Admin";
	}

}
