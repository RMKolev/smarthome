package users;

public class User implements java.io.Serializable {

	private static final long serialVersionUID = 1565020101501169516L;
	private String name;

	public User(String name) {
		this.name = name;
	}

	public User() {
		this.name = "Guest";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public String getTitle() {
		return "Guest";
	}

	public String getName() {
		return this.name;
	}
}
