package users;

public class RegisteredUser extends User {
	private static final long serialVersionUID = 5467420500384022900L;
	private String password;

	public RegisteredUser(String name, String password) {
		super(name);
		this.password = password;
	}

	public boolean validPassword(String password) {
		if (this.password.equals(password)) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegisteredUser other = (RegisteredUser) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String getTitle() {
		return "RegisteredUser";
	}

}
