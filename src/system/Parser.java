package system;

import commandfactory.Command;
import commandfactory.CommandFactory;
import exceptions.DatabaseException;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;

public class Parser {
	public Command parseCommand(String c, String priviledge)
			throws InvalidPriviledgeException, InvalidParameterException, DatabaseException {
		String[] commandParts = c.split(" ", 2);
		if (commandParts.length > 1) {
			return CommandFactory.get(commandParts[0], commandParts[1], priviledge);
		} else {
			return CommandFactory.get(commandParts[0], "", priviledge);
		}
	}
}
