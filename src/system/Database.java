package system;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class Database<K, V> {
	protected HashMap<K, V> database;

	public Database() {
		this.database = new HashMap<K, V>();
	}

	protected void updateDatabase(String name) {
		File file = new File(name);
		try (ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream(file, false))) {
			fout.writeObject(this.database);
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	protected void getDatabaseFromFile(String name) {
		try (ObjectInputStream f = new ObjectInputStream(new FileInputStream(name))) {
			this.database = (HashMap<K, V>) f.readObject();
		} catch (FileNotFoundException e) {
			System.out.println("No database file found.Starting a new Database at " + name);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
