package system;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;

import exceptions.DatabaseException;
import users.Admin;
import users.RegisteredUser;
import users.User;

public class UserDatabase extends Database<String, RegisteredUser> {

	UserDatabase() {
		super();
		this.getDatabaseFromFile("UserDatabase.ser");
		if (!this.containsUser("Root42", "whoami")) {
			this.database.put("Root42", new Admin("Root42", "whoami"));
		}
	}

	void addUser(RegisteredUser newUser) throws DatabaseException {
		if (this.database.containsKey(newUser.getName()))
			throw new DatabaseException();
		else {
			System.out.println(newUser.getName() + " being added to the user list");
			this.database.put(newUser.getName(), newUser);
			this.updateDatabase("UserDatabase.ser");
		}
	}

	void deleteUser(String name) throws DatabaseException {
		if (!this.database.containsKey(name)) {
			throw new DatabaseException("This user doesn't exist");
		} else {
			System.out.println("Removing user");
			this.database.remove(name);
			this.updateDatabase("UserDatabase");
		}
	}

	boolean containsUser(String name, String password) {
		if (this.database.containsKey(name)) {
			return this.database.get(name).validPassword(password);
		}
		return false;
	}

	RegisteredUser getUser(String name, String password) throws DatabaseException {
		if (this.containsUser(name, password)) {
			return this.database.get(name);
		} else
			throw new DatabaseException("User does not exist");
	}

	void printUserlist() {
		Iterator<RegisteredUser> it = this.database.values().iterator();
		while (it.hasNext()) {
			User u = it.next();
			System.out.println(u.getTitle() + ":" + u.getName());
		}
	}

	// Function to be edited when new functionality on users gets added, since now
	// it doesn't matter that much. Admins will never be able to see
	// passwords,though!
	void printUser(String name, String priviledge) throws DatabaseException {
		if (priviledge.equals("RegisteredUser")) {
			User u = this.database.get(name);
			if (u != null) {
				System.out.println(u.getName() + ":" + u.getTitle());
			} else {
				throw new DatabaseException("User not found");
			}
		} else {
			User u = this.database.get(name);
			if (u != null) {
				System.out.println(u.getName() + ":" + u.getTitle());
			} else {
				throw new DatabaseException("User not found");
			}
		}
	}
}
