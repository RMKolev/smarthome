package system;

import java.util.Scanner;

import commandfactory.Command;
import exceptions.DatabaseException;
import exceptions.InvalidParameterException;
import exceptions.InvalidPriviledgeException;
import exceptions.SystemException;
import users.Admin;
import users.RegisteredUser;
import users.User;

public class UserSystem {
	private UserDatabase users;
	private BuildingDatabase buildings;
	private double priceWater = 0.0;
	private double priceElectricity = 0.0;
	private User currentUser = new User();

	public UserSystem() {
		this.users = new UserDatabase();
		this.buildings = new BuildingDatabase();
	}

	public void setPriceWater(double price) throws SystemException {
		if (price < 0) {
			throw new SystemException("Invalid price set");
		}
	}

	public void setPriceElectricity(double price) throws SystemException {
		if (price < 0) {
			throw new SystemException("Invalid price set");
		}
	}

	public void addUserToDatabase(String name, String password) throws DatabaseException {
		System.out.println("Adding user");
		this.users.addUser(new RegisteredUser(name, password));
	}

	public void removeCurrentUserFromDatabase() throws SystemException {
		this.users.deleteUser(this.currentUser.getName());
		this.logOutUser();
	}

	public void addAdminToDatabase(String name, String password) throws DatabaseException {
		this.users.addUser(new Admin(name, password));
	}

	public void printDatabase() {
		this.users.printUserlist();
	}

	public String getCurrentUserName() {
		return this.currentUser.getName();
	}

	public void logOutUser() {
		this.currentUser = new User();
	}

	public void logInUser(String name, String password) throws DatabaseException {
		System.out.println("Signing in");
		RegisteredUser newUser = this.users.getUser(name, password);
		if (newUser == null)
			return; // to Throw InvalidCredentials
		else {
			this.currentUser = newUser;
		}
	}

	public void displayUser(String priviledge) {
		if (priviledge.equals("Admin")) {

		}
	}

	public void init() {
		Parser p = new Parser();
		Scanner kb = new Scanner(System.in);
		while (true) {
			System.out.println("Welcome," + this.currentUser.getName());
			String line = kb.nextLine();
			try {
				Command c = p.parseCommand(line, currentUser.getTitle());
				c.execute(this);
			} catch (SystemException e) {
				System.out.println(e.getMessage());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}

	public void showUser(String name) throws DatabaseException {
		this.users.printUser(name, this.currentUser.getTitle());
	}

	public void addBuilding(String name) throws DatabaseException {
		System.out.println("Adding building");
		if (this.buildings.contains(name)) {
			throw new DatabaseException("Building already exists in the database");
		} else {
			this.buildings.addBuilding(name);
		}
	}

	public void addSmartDevice(String buildingName, String deviceName, String deviceType) throws DatabaseException {
		if (!this.buildings.contains(buildingName)) {
			throw new DatabaseException("No such building in the database");
		} else {
			System.out.println("Device getting added " + deviceName);
			this.buildings.addDevice(buildingName, deviceName, deviceType);
		}
	}

	public void printAllBuildings() {
		this.buildings.printBuildings();
	}

	public void displayAllDevices() {
		this.buildings.print();

	}

	public void showBill(String name, String type) {
		this.buildings.showBill(name, type);

	}

	public void setBill(String type, double price) {
		if (type.equals("water")) {
			this.priceWater = price;
		} else if (type.equals("electricity")) {
			this.priceElectricity = price;
		}
	}

	public void movePeriod(int periodCount) {
		this.buildings.charge(priceWater, periodCount, "water");
		this.buildings.charge(priceElectricity, periodCount, "electricity");
		this.buildings.update();
	}
}
