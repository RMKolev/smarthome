package system;

import buildings.Building;
import buildings.Chargeable;

public class BuildingDatabase extends Database<String, Building> implements Chargeable {

	public BuildingDatabase() {
		super();
		this.getDatabaseFromFile("BuildingsDatabase.ser");
	}
	public void update() {
		this.updateDatabase("BuildingsDatabase.ser");
	}
	public void addBuilding(String name) {
		if (!this.database.containsKey(name)) {
			this.database.put(name, new Building(name));
			update();
		}
	}

	public void addDevice(String buildingName, String deviceName, String deviceType) {
		if (this.database.containsKey(buildingName)) {
			System.out.println(deviceName + " type:" + deviceType);
			this.database.get(buildingName).addDevice(deviceName, deviceType);
			update();
		}
	}

	public Building getBuilding(String name) {
		return this.database.get(name);
	}

	@Override
	public void charge(double price, int periodCount, String type) {
		this.database.forEach((k, v) -> v.charge(price, periodCount, type));
	}

	public void printBuildings() {
		this.database.forEach((k, v) -> System.out.println(v.getName()));
	}

	public void print() {
		this.database.forEach((k, v) -> v.print());
	}

	public void showDevice(String name) {
		this.database.forEach((k, v) -> {
			if (k.equals(name)) {
				v.print();
			}
		});
	}

	public boolean contains(String name) {
		return this.database.containsKey(name);
	}

	public void showBill(String name, String type) {
		this.database.forEach((k,v) ->{
			if(k.equals(name)) {
				System.out.println(k + ":" + v.printSum(type));
			}
			v.showDeviceBill(name,type);
			});
		}
}
